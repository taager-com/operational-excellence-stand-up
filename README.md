# Team stand-up

This is a tool that stand-up meetings at Taager more efficient.

## App structure

The app was written in Kotlin, using Kotlin Multiplatform (KMP). It targets Windows, Linux and MacOS.
Currently there is a single screen (in `Main.kt`), using `MainViewModel`. The screen communicates with the view model via view events.

Taager's Circuit library to be added later.

## Running the app

The recommended IDE is IntelliJ IDEA.

Once the repository is cloned and opened, run the `main()` function in `Main.kt`.

## Build the executable

As it's the case with Kotlin Multiplatform Desktop projects, you can create an executable that runs directly in your OS.

Among the Gradle tasks, you will find the `compose desktop/package(Deb|Dmg|Msi)` tasks for Linux, MacOS and Windows respectively. Run the task that suits your OS and find the executable in in the `build/compose/binaries` folder.

## Using the app

When running the app, a countdown timer and the list of participants is displayed.

The timer counts down to the current hour's 46 minutes and 30 seconds. It marks the start of the meeting, taking into consideration late joiners.

The present members can be selected by clicking in the member icon next to each name. The same method is used to remove someone from being present.

When the timer reaches 0 or the Start button is clicked, the members are shuffled and the first speaker's name is visible on top, accompanied by the next one. If you'd like to add further members, click the darker area with the chevron at the bottom of the window. This will expand and show the members that were marked as non-present when the meeting started. They will be added to a random position in the list.

There is a 60-second timeout set up for each speaker. This timer needs to be started for the first speaker, and will start automatically for all others.
