sealed class MainViewEvent {
    object Init : MainViewEvent()
    data class TogglePresence(val member: String) : MainViewEvent()
    object FinishTalking : MainViewEvent()
    object Start : MainViewEvent()
    object NextClicked : MainViewEvent()
    object ToggleNonPresentMembersVisible : MainViewEvent()
}
