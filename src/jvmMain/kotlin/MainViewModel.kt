import korlibs.time.DateTime
import korlibs.time.minutes
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class MainViewModel {
    private val _state: MutableStateFlow<MainScreenState> = MutableStateFlow(MainScreenState())
    val state: StateFlow<MainScreenState> = _state

    private val scope = CoroutineScope(Dispatchers.Default)
    private var timerJob: Job? = null
    private var startCountdownJob: Job? = null

    fun onEvent(event: MainViewEvent) {
        when (event) {
            MainViewEvent.Init -> init()
            MainViewEvent.FinishTalking -> onFinishTalking()
            is MainViewEvent.TogglePresence -> onTogglePresence(event.member)
            MainViewEvent.Start -> onStart()
            MainViewEvent.NextClicked -> onNextClicked()
            MainViewEvent.ToggleNonPresentMembersVisible -> onToggleNonPresentMembersVisible()
        }
    }

    private fun init() {
        updateState(
            state.value.copy(
                membersNotPresent = memberNames.sorted(),
            )
        )
        startCountdownToStart()
    }

    private fun startCountdownToStart() {
        startCountdownJob = scope.launch {
            val plannedStart = DateTime.now().startOfHour.plus(46.5.minutes)
            val startTime = if (plannedStart < DateTime.now().plus(1.minutes)) {
                DateTime.now().plus(1.minutes)
            } else {
                plannedStart
            }
            while (DateTime.now() < startTime) {
                val timeLeftInSeconds = ((startTime.unixMillis - DateTime.now().unixMillis) / 1000).toLong()
                val minutesLeft = String.format("%02d", timeLeftInSeconds / 60)
                val secondsLeft = String.format("%02d", timeLeftInSeconds % 60)
                updateState(
                    state.value.copy(
                        timeLeftUntilStart = "$minutesLeft:$secondsLeft",
                    )
                )
                delay(50)
                if (startCountdownJob?.isActive == false) return@launch
            }
            updateState(
                state.value.copy(
                    timeLeftUntilStart = "00:00",
                )
            )
            if (state.value.membersPresent.isNotEmpty()) {
                onStart()
            }
        }
    }

    private fun onFinishTalking() {
        val membersPresent = state.value.membersPresent
        val nowTalking = state.value.talking
        val memberIndex: Int = if (nowTalking == null) {
            -1
        } else {
            membersPresent.indexOf(nowTalking)
        }
        val lastIndex = membersPresent.lastIndex
        val nextTalking = if (memberIndex < lastIndex) {
            membersPresent[memberIndex]
        } else {
            null
        }

        updateState(
            state.value.copy(
                talking = nextTalking,
            )
        )
    }

    private fun onTogglePresence(member: String) {
        val membersPresent = state.value.membersPresent
        val membersNotPresent = state.value.membersNotPresent
        val (updatedMembersPresent, updatedMembersNotPresent) = when {
            state.value.membersPresent.contains(member) -> {
                Pair(
                    membersPresent.filter { it != member },
                    membersNotPresent + member,
                )
            }
            else -> {
                val updatedMembersPresent = if (state.value.didStart) {
                    if (state.value.next == null) {
                        updateState(
                            state.value.copy(
                                next = member,
                            )
                        )
                        membersPresent + member
                    } else {
                        val talkingIndex = state.value.membersPresent.indexOf(state.value.talking)
                        val insertIndex =
                            ((talkingIndex + 2)..(state.value.membersPresent.lastIndex + 1)).toList().random()
                        val updatedMembersPresent = state.value.membersPresent.toMutableList()
                        updatedMembersPresent.add(insertIndex, member)
                        updatedMembersPresent.toList()
                    }
                } else {
                    membersPresent + member
                }
                Pair(
                    updatedMembersPresent,
                    membersNotPresent.filter { it != member },
                )
            }
        }
        updateState(
            state.value.copy(
                membersPresent = updatedMembersPresent,
                membersNotPresent = updatedMembersNotPresent,
                isStartButtonEnabled = updatedMembersPresent.isNotEmpty(),
            )
        )
    }

    private fun onStart() {
        startCountdownJob?.cancel()
        val shuffledMembers = state.value.membersPresent.shuffled()
        updateState(
            state.value.copy(
                didStart = true,
                membersPresent = shuffledMembers,
                talking = shuffledMembers.first(),
                next = shuffledMembers.getOrNull(1)
            )
        )
    }

    private fun startTimer() {
        timerJob?.cancel()
        val timeLimit = 60 * 1000
        timerJob = scope.launch {
            val timeUp = System.currentTimeMillis() + timeLimit
            while (timeUp > System.currentTimeMillis()) {
                val timeLeftScale = (timeUp - System.currentTimeMillis()) / timeLimit.toDouble()
                updateState(
                    state.value.copy(
                        timeLeft = timeLeftScale,
                    )
                )
                delay(20)
            }
            updateState(
                state.value.copy(
                    timeLeft = 0.0,
                )
            )
        }
    }

    private fun onNextClicked() {
        if (!state.value.isTimerStarted) {
            updateState(
                state.value.copy(
                    isTimerStarted = true,
                )
            )
            startTimer()
            return
        }

        val members = state.value.membersPresent
        val nextIndex = state.value.membersPresent.indexOf(state.value.next)
        val next = when {
            nextIndex < 0 -> null
            nextIndex < state.value.membersPresent.lastIndex -> members[nextIndex + 1]
            else -> null
        }

        updateState(
            state.value.copy(
                talking = state.value.next,
                next = next,
            )
        )
        startTimer()
    }

    private fun onToggleNonPresentMembersVisible() {
        updateState(
            state.value.copy(
                nonPresentMembersVisible = !state.value.nonPresentMembersVisible,
            )
        )
    }

    private fun updateState(newState: MainScreenState) {
        _state.value = newState
    }
}

//private val memberNames = listOf()
private val memberNames = (0..7).map { "Member $it" }
