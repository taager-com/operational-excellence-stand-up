import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.geometry.center
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.WindowPosition
import androidx.compose.ui.window.WindowState
import androidx.compose.ui.window.application
import kotlin.math.min

@Composable
@Preview
private fun App(viewModel: MainViewModel) {
    MaterialTheme {
        val _state = viewModel.state.collectAsState(MainScreenState())
        val state = _state.value

        LaunchedEffect(true) {
            viewModel.onEvent(MainViewEvent.Init)
        }

        val onEvent = viewModel::onEvent

        if (!state.didStart) {
            MemberSelector(
                startingIn = state.timeLeftUntilStart,
                membersPresent = state.membersPresent,
                membersNotPresent = state.membersNotPresent,
                isStartButtonEnabled = state.isStartButtonEnabled,
                onPresenceToggle = { onEvent(MainViewEvent.TogglePresence(it)) },
                onStart = { onEvent(MainViewEvent.Start) },
            )
        } else {
            RunningSession(
                talking = state.talking,
                next = state.next,
                isTimerStarted = state.isTimerStarted,
                timeLeft = state.timeLeft,
                nonPresentMembers = state.membersNotPresent,
                nonPresentMembersVisible = state.nonPresentMembersVisible,
                onNextClicked = { onEvent(MainViewEvent.NextClicked) },
                onToggleNonPresentMembers = { onEvent(MainViewEvent.ToggleNonPresentMembersVisible) },
                onTogglePresence = { onEvent(MainViewEvent.TogglePresence(it)) },
            )
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun MemberSelector(
    startingIn: String,
    membersPresent: List<String>,
    membersNotPresent: List<String>,
    isStartButtonEnabled: Boolean,
    onPresenceToggle: (String) -> Unit,
    onStart: () -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color(0xFFEFEFEF))
            .padding(24.dp),
        verticalArrangement = Arrangement.spacedBy(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Text(
            text = "Starting in",
            style = TextStyle(
                fontSize = 40.sp,
                fontWeight = FontWeight.Bold,
            ),
        )
        Text(
            text = startingIn,
            style = TextStyle(
                fontSize = 80.sp,
                fontWeight = FontWeight.Bold,
            ),
        )

        Spacer(modifier = Modifier.height(24.dp))

        LazyColumn(
            modifier = Modifier
                .weight(1f)
                .fillMaxWidth(),
            verticalArrangement = Arrangement.spacedBy(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {

            // Present members
            itemsIndexed(
                items = membersPresent,
                key = { _, member -> member }
            ) { _, member ->
                MemberCard(
                    member = member,
                    onPresenceToggle = { onPresenceToggle(member) },
                    modifier = Modifier.animateItemPlacement(),
                    isPresent = true,
                    didTalk = false,
                    isTalking = false,
                )
            }

            item {
                Spacer(modifier = Modifier.height(24.dp))
            }

            // Non-present members
            itemsIndexed(
                items = membersNotPresent,
                key = { _, member -> member }
            ) { _, member ->
                MemberCard(
                    member = member,
                    onPresenceToggle = { onPresenceToggle(member) },
                    modifier = Modifier.animateItemPlacement(),
                    isPresent = false,
                    didTalk = false,
                    isTalking = false,
                )
            }
        }

        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.spacedBy(16.dp),
        ) {
            Button(
                onClick = onStart,
                modifier = Modifier.weight(1f),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = Color(0xFF00E676),
                    contentColor = Color.White,
                    disabledBackgroundColor = Color.LightGray,
                ),
                enabled = isStartButtonEnabled
            ) {
                Row(
                    horizontalArrangement = Arrangement.spacedBy(8.dp),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Icon(
                        imageVector = Icons.Filled.PlayArrow,
                        contentDescription = null,
                        modifier = Modifier.size(32.dp),
                    )
                    Text(
                        text = "Start",
                        style = TextStyle(
                            fontSize = 24.sp,
                            fontWeight = FontWeight.Bold,
                        )
                    )
                }
            }
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun RunningSession(
    talking: String?,
    next: String?,
    isTimerStarted: Boolean,
    timeLeft: Double,
    nonPresentMembers: List<String>,
    nonPresentMembersVisible: Boolean,
    onNextClicked: () -> Unit,
    onToggleNonPresentMembers: () -> Unit,
    onTogglePresence: (String) -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color(0xFFEFEFEF))
    ) {
        LazyColumn(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
                .padding(24.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(24.dp),
        ) {
            talking?.let {
                item {
                    NowTalkingCard(
                        member = talking,
                        isTimerStarted = isTimerStarted,
                        timeLeft = timeLeft,
                        isNextButtonVisible = next != null,
                        onNextClicked = onNextClicked,
                    )
                }
            }

            next?.let {
                item {
                    NextCard(
                        member = next,
                    )
                }
            }
        }
        Box(
            modifier = Modifier
                .height(20.dp)
                .fillMaxWidth()
                .background(Color(0x11000000))
                .clickable { onToggleNonPresentMembers() }
        ) {
            val rotation by animateFloatAsState(if (nonPresentMembersVisible) 180f else 0f)

            Icon(
                imageVector = Icons.Filled.KeyboardArrowUp,
                contentDescription = null,
                modifier = Modifier
                    .size(16.dp)
                    .rotate(rotation)
                    .align(Alignment.Center),
            )
        }

        AnimatedVisibility(visible = nonPresentMembersVisible) {
            LazyColumn(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(320.dp)
                    .background(Color(0x11000000)),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.spacedBy(8.dp),
            ) {
                itemsIndexed(
                    items = nonPresentMembers,
                    key = { _, member -> member }
                ) { _, member ->
                    MemberCard(
                        modifier = Modifier
                            .animateItemPlacement()
                            .fillMaxWidth(0.8f),
                        member = member,
                        isPresent = false,
                        isTalking = false,
                        didTalk = false,
                        onPresenceToggle = { onTogglePresence(member) },
                    )
                }
            }
        }
    }
}

private val cornerSize = 8.dp
private val strokeWidth = 8.dp

@Composable
private fun NowTalkingCard(
    member: String,
    isTimerStarted: Boolean,
    timeLeft: Double,
    isNextButtonVisible: Boolean,
    onNextClicked: () -> Unit,
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .clip(RoundedCornerShape(cornerSize))
            .background(Color.White)
            .drawBehind {
                if (timeLeft == 0.0) return@drawBehind

                val green = Color(0xFF00E676)
                val cornerSizePx = cornerSize.toPx()
                val horizontalLength = size.width - 2 * cornerSizePx
                val verticalLength = size.height - 2 * cornerSizePx
                val cornerLength = (cornerSizePx * 2 * Math.PI / 4).toFloat()
                val totalLength = 2 * horizontalLength +
                        2 * verticalLength +
                        cornerSizePx * 2 * Math.PI

                val path = Path().apply {
                    val topLeftSegmentLengthScale = (horizontalLength / 2.0) / totalLength
                    moveTo(size.center.x, 0f)
                    lineTo(
                        (center.x - (min(topLeftSegmentLengthScale, timeLeft) / topLeftSegmentLengthScale) * (horizontalLength / 2f)).toFloat(),
                        0f
                    )

                    val cornerLengthScale = cornerLength / totalLength
                    var lengthDrawnSoFar = topLeftSegmentLengthScale

                    if (timeLeft > lengthDrawnSoFar) {
                        arcTo(
                            rect = Rect(0f, 0f, cornerSizePx * 2, cornerSizePx * 2),
                            startAngleDegrees = 270f,
                            sweepAngleDegrees = -90f * (min(timeLeft - lengthDrawnSoFar, cornerLengthScale) / cornerLengthScale).toFloat(),
                            forceMoveTo = false,
                        )
                        lengthDrawnSoFar += cornerLengthScale
                    }

                    if (timeLeft > lengthDrawnSoFar) {
                        val leftSegmentLengthScale = verticalLength / totalLength
                        lineTo(
                            0f,
                            cornerSizePx + verticalLength * (min(timeLeft - lengthDrawnSoFar, leftSegmentLengthScale) / leftSegmentLengthScale).toFloat()
                        )
                        lengthDrawnSoFar += leftSegmentLengthScale
                    }

                    if (timeLeft > lengthDrawnSoFar) {
                        arcTo(
                            rect = Rect(0f, size.height - cornerSizePx * 2, cornerSizePx * 2, size.height),
                            startAngleDegrees = 180f,
                            sweepAngleDegrees = -90f * (min(timeLeft - lengthDrawnSoFar, cornerLengthScale) / cornerLengthScale).toFloat(),
                            forceMoveTo = false,
                        )
                        lengthDrawnSoFar += cornerLengthScale
                    }

                    if (timeLeft > lengthDrawnSoFar) {
                        val bottomSegmentLengthScale = horizontalLength / totalLength
                        lineTo(
                            cornerSizePx + horizontalLength * (min(timeLeft - lengthDrawnSoFar, bottomSegmentLengthScale) / bottomSegmentLengthScale).toFloat(),
                            size.height
                        )
                        lengthDrawnSoFar += bottomSegmentLengthScale
                    }

                    if (timeLeft > lengthDrawnSoFar) {
                        arcTo(
                            rect = Rect(size.width - cornerSizePx * 2, size.height - cornerSizePx * 2, size.width, size.height),
                            startAngleDegrees = 90f,
                            sweepAngleDegrees = -90f * (min(timeLeft - lengthDrawnSoFar, cornerLengthScale) / cornerLengthScale).toFloat(),
                            forceMoveTo = false,
                        )
                        lengthDrawnSoFar += cornerLengthScale
                    }

                    if (timeLeft > lengthDrawnSoFar) {
                        val rightSegmentLengthScale = verticalLength / totalLength
                        lineTo(
                            size.width,
                            size.height - cornerSizePx - verticalLength * (min(timeLeft - lengthDrawnSoFar, rightSegmentLengthScale) / rightSegmentLengthScale).toFloat()
                        )
                        lengthDrawnSoFar += rightSegmentLengthScale
                    }

                    if (timeLeft > lengthDrawnSoFar) {
                        arcTo(
                            rect = Rect(size.width - cornerSizePx * 2, 0f, size.width, cornerSizePx * 2),
                            startAngleDegrees = 0f,
                            sweepAngleDegrees = -90f * (min(timeLeft - lengthDrawnSoFar, cornerLengthScale) / cornerLengthScale).toFloat(),
                            forceMoveTo = false,
                        )
                        lengthDrawnSoFar += cornerLengthScale
                    }

                    if (timeLeft > lengthDrawnSoFar) {
                        val topRightSegmentLengthScale = (horizontalLength / 2.0) / totalLength
                        lineTo(
                            (size.width - cornerSizePx - (min(timeLeft - lengthDrawnSoFar, topRightSegmentLengthScale) / topRightSegmentLengthScale) * (horizontalLength / 2f)).toFloat(),
                            0f
                        )
                    }
                }

                drawPath(
                    path = path,
                    color = green,
                    style = Stroke(strokeWidth.toPx(), cap = StrokeCap.Round),
                )
            }
            .padding(24.dp)
    ) {
        Text(
            text = "Now talking",
            style = TextStyle(
                fontSize = 20.sp,
            ),
            color = Color.DarkGray,
        )
        Spacer(modifier = Modifier.height(24.dp))
        Text(
            text = member,
            style = TextStyle(
                fontSize = 40.sp,
                fontWeight = FontWeight.Bold,
            )
        )
        Spacer(modifier = Modifier.height(32.dp))
        Button(
            modifier = Modifier.fillMaxWidth(),
            onClick = onNextClicked,
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color(0xFF00E676),
                contentColor = Color.White,
                disabledBackgroundColor = Color.LightGray,
            ),
        ) {
            Row(
                horizontalArrangement = Arrangement.spacedBy(8.dp),
                verticalAlignment = Alignment.CenterVertically,
            ) {
                when {
                    !isTimerStarted -> {
                        Icon(
                            imageVector = Icons.Filled.PlayArrow,
                            contentDescription = null,
                            modifier = Modifier.size(32.dp),
                        )
                        Text(
                            text = "Start timer",
                            style = TextStyle(
                                fontSize = 24.sp,
                                fontWeight = FontWeight.Bold,
                            )
                        )
                    }

                    isNextButtonVisible -> {
                        Text(
                            text = "Next",
                            style = TextStyle(
                                fontSize = 24.sp,
                                fontWeight = FontWeight.Bold,
                            )
                        )
                        Icon(
                            imageVector = Icons.Filled.ArrowForward,
                            contentDescription = null,
                            modifier = Modifier.size(32.dp),
                        )
                    }

                    else -> {
                        Icon(
                            imageVector = Icons.Filled.Check,
                            contentDescription = null,
                            modifier = Modifier.size(32.dp),
                        )
                        Text(
                            text = "Finish",
                            style = TextStyle(
                                fontSize = 24.sp,
                                fontWeight = FontWeight.Bold,
                            )
                        )
                    }
                }
            }
        }
    }
}

@Composable
private fun NextCard(
    member: String,
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxWidth(0.8f)
            .clip(RoundedCornerShape(8.dp))
            .background(Color.White)
            .border(
                width = 1.dp,
                color = Color.LightGray,
                shape = RoundedCornerShape(8.dp),
            )
            .padding(24.dp)
    ) {
        Text(
            text = "Next up",
            style = TextStyle(
                fontSize = 16.sp,
            ),
            color = Color.DarkGray,
        )
        Spacer(modifier = Modifier.height(24.dp))
        Text(
            text = member,
            style = TextStyle(
                fontSize = 24.sp,
                fontWeight = FontWeight.Bold,
            )
        )
    }
}

@Composable
private fun MemberCard(
    modifier: Modifier = Modifier,
    member: String,
    isPresent: Boolean,
    isTalking: Boolean,
    didTalk: Boolean,
    onPresenceToggle: () -> Unit,
    onFinishTalking: () -> Unit = {},
) {
    val alpha by animateFloatAsState(
        when {
            !isPresent -> 0.3f
            didTalk -> 0.7f
            else -> 1f
        }
    )
    val widthScale by animateFloatAsState(if (isTalking) 1f else 0.8f)
    val padding by animateDpAsState(if (isTalking) 32.dp else 16.dp)
    val borderWidth by animateDpAsState(if (isTalking) 3.dp else 1.dp)
    val borderColor by animateColorAsState(if (isTalking) Color(0xFF4caf50) else Color.Gray)
    val backgroundColor by animateColorAsState(if (isTalking) Color(0xFFE8F5E9) else Color.White)

    Card(
        modifier = modifier
            .fillMaxWidth(widthScale)
            .clip(RoundedCornerShape(8.dp))
            .background(backgroundColor)
            .border(
                width = borderWidth,
                color = borderColor,
                shape = RoundedCornerShape(8.dp),
            )
            .alpha(alpha),
        elevation = 4.dp,
    ) {
        Row(
            modifier = Modifier.padding(
                horizontal = padding,
                vertical = 12.dp,
            ),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Icon(
                imageVector = Icons.Filled.Person,
                contentDescription = null,
                tint = Color.DarkGray,
                modifier = Modifier
                    .size(36.dp)
                    .clickable { onPresenceToggle() },
            )
            Spacer(modifier = Modifier.width(8.dp))
            Text(
                text = member,
                style = TextStyle(
                    fontSize = 24.sp,
                    fontWeight = FontWeight.Bold,
                )
            )
            if (isTalking) {
                Spacer(modifier = Modifier.weight(1f))
                Icon(
                    imageVector = Icons.Filled.Check,
                    contentDescription = null,
                    modifier = Modifier
                        .size(48.dp)
                        .clickable { onFinishTalking() },
                )
            }
        }
    }
}

fun main() = application {
    Window(
        onCloseRequest = ::exitApplication,
        state = WindowState(
            width = 400.dp,
            height = 1080.dp,
        ),
        title = "Ops stand-up",
    ) {
        App(MainViewModel())
    }
}
