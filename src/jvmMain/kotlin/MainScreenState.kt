data class MainScreenState(
    val timeLeftUntilStart: String = "",
    val membersPresent: List<String> = emptyList(),
    val membersNotPresent: List<String> = emptyList(),
    val isTimerStarted: Boolean = false,
    val timeLeft: Double = 1.0,
    val didTalk: List<String> = emptyList(),
    val talking: String? = null,
    val next: String? = null,
    val didStart: Boolean = false,
    val isStartButtonEnabled: Boolean = false,
    val isNotPresentMembersVisible: Boolean = false,
    val nonPresentMembersVisible: Boolean = false,
)
